const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
app.use(express.static(__dirname));

// support json encoded bodies
app.use(bodyParser.json()); 

// Data of the application
var usersDone = [
  { "nickname": "Daniel", "points": "100" }
];

var points = [
    { "id": "0", "value": "100" }
];

// the GET "usersDone" API endpoint
app.get('/api/usersdone', function (req, res) {

    console.log("GET users list than has obtained points for free");
    res.send(usersDone);
});

// the GET "points" API endpoint
app.get('/api/points', function (req, res) {

    console.log("GET current value of number of points to give");
    res.send(points);
});

// POST endpoint for creating a new user register
app.post('/api/usersdone', function (req, res) {
	
    console.log("POST userDone, the user: " + req.body.nickname + " has obtained his points for free");
    let new_userDone = { "nickname": req.body.nickname, "points": req.body.points };
    usersDone.push(new_userDone);
    res.send(new_userDone);
});

// PUT endpoint for editing the current value of number of free points given
app.put('/api/points/:id', function (req, res) {

    console.log("PUT points value, new value : " + req.body.value);
    let id = req.params.id;
    let f = points.find(x => x.id == id);
    f.value = req.body.value;
    res.send(f);
});

// catch 404 error and forward to error handler
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// HTTP listener
app.listen(3000, function () {
    console.log('API for EjemploMinijuegoPIIS listening on port 3000!');
});
module.exports = app;