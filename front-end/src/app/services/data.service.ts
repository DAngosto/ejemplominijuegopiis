import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
 
const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
 
@Injectable()
export class DataService {
 
    constructor(private http:HttpClient) {}
 
    getUsersDone() {
        return this.http.get('/api/usersdone');
    }

    getPointsValue() {
        return this.http.get('/api/points');
    }

    updatepointsValue(value) {
        let body = JSON.stringify(value);
        return this.http.put('/api/points/0', body, httpOptions);
    }

    createUserDone(user) {
        let body = JSON.stringify(user);
         return this.http.post('/api/usersdone/', body, httpOptions);
    }

}// END OF DATA SERVICE