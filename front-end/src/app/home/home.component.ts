import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: []
  
})
export class HomeComponent implements OnInit {

  private swapAnimations : boolean = false;
  private hideAnimation : boolean = false;
  private inputNickname;
  public puntos;

  constructor(private _dataService: DataService) { }  

   ngOnInit(): void { 
     this.getPointsValue();
   } 

   sawConfirmation(){
    this.swapAnimations = true;
    this.hideAnimation = true;
    setTimeout(() => {
      this.swapAnimations = false;
      this.hideAnimation = false;
    }, 1600)
  }

  getPointsValue() {
    this._dataService.getPointsValue().subscribe(
       data => { this.puntos = data},
       err => console.error(err)
     );
  }

  givePointsToUser(nickname) {
    var currentPoints = 0;
    this.puntos.forEach(function (value) {
        currentPoints = value.value;
    });
    let user = {"nickname": nickname, "points": currentPoints };
    this._dataService.createUserDone(user).subscribe(
        data => {
             this.getPointsValue();
             this.resetInputvalue();
             return true;
           },
           error => {
             console.error("Error saving the user who wanted free points!");
             return Observable.throw(error);
           }
    );
    this.sawConfirmation();
  }

  resetInputvalue(){
    this.inputNickname = "";
  }

}// END OF HOME COMPONENT
