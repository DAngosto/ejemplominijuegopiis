import { Component, OnInit } from '@angular/core';
import {DataService} from '../services/data.service';
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  providers: [] 
})

export class AdminComponent implements OnInit {


  private currentPointsValue: number;
  private inputNewValue;
  public users;
  public puntos;

  constructor(private _dataService: DataService) { }  

  ngOnInit(): void { 
     this.getUsersDone();
     this.getPointsValue();
  } 

  setPointsValue(value){
    let points = {"value": value};
    this._dataService.updatepointsValue(points).subscribe(
             data => {
               this.getPointsValue();
               this.getUsersDone();
               return true;
             },
             error => {
               console.error("Error updating value of points!");
               return Observable.throw(error);
             }
    );
    this.resetInputvalue();
  }


  getUsersDone() {
    this._dataService.getUsersDone().subscribe(
       data => { this.users = data},
       err => console.error(err)
     );
  }

  getPointsValue() {
    this._dataService.getPointsValue().subscribe(
       data => { this.puntos = data},
       err => console.error(err)
     );
  }

  resetInputvalue(){
    this.inputNewValue = "";
  }

}// END OF ADMIN COMPONENT
